package poly_calc;

/*
 * Katona David - 30224
 */
public class Operations {

	private int i = 0, j = 0; // iterators
	private Polynomial result = new Polynomial(); // final result
	private Monomial monom; // auxiliary Monomial type object

	public Polynomial add(Polynomial P, Polynomial Q) {

		i = 0;
		j = 0;
		result.cleanMembers();
		int nrP = P.nrOfmembers(), nrQ = Q.nrOfmembers();

		while (i < nrP && j < nrQ) {
			double coefP = P.getPos(i).getCoef(), powP = P.getPos(i).getPower();
			double coefQ = Q.getPos(j).getCoef(), powQ = Q.getPos(j).getPower();
			if (powP == powQ) {
				monom = new Monomial(coefP + coefQ, powP);
				i++;
				j++;
			} else if (powP > powQ) {
				monom = new Monomial(coefP, powP);
				i++;
			} else {
				monom = new Monomial(coefQ, powQ);
				j++;
			}
			result.addMembers(monom);
		}
		while (i < nrP) {
			double coefP = P.getPos(i).getCoef(), powP = P.getPos(i).getPower();
			monom = new Monomial(coefP, powP);
			i++;
			result.addMembers(monom);
		}
		while (j < nrQ) {
			double coefQ = Q.getPos(j).getCoef(), powQ = Q.getPos(j).getPower();
			monom = new Monomial(coefQ, powQ);
			j++;
			result.addMembers(monom);
		}

		return result;
	}

	public Polynomial sub(Polynomial P, Polynomial Q) {

		i = 0;
		j = 0;
		result.cleanMembers();
		int nrP = P.nrOfmembers(), nrQ = Q.nrOfmembers();

		while (i < nrP && j < nrQ) {
			double coefP = P.getPos(i).getCoef(), powP = P.getPos(i).getPower();
			double coefQ = Q.getPos(j).getCoef(), powQ = Q.getPos(j).getPower();
			if (powP == powQ) {
				monom = new Monomial(coefP - coefQ, powP);
				i++;
				j++;
			} else if (powP < powQ) {
				monom = new Monomial(-coefQ, powQ);
				j++;
			} else {
				monom = new Monomial(coefP, powP);
				i++;
			}
			result.addMembers(monom);
		}
		while (i < nrP) {
			double coefP = P.getPos(i).getCoef(), powP = P.getPos(i).getPower();
			monom = new Monomial(coefP, powP);
			i++;
			result.addMembers(monom);
		}
		while (j < nrQ) {
			double coefQ = Q.getPos(j).getCoef(), powQ = Q.getPos(j).getPower();
			monom = new Monomial(-coefQ, powQ);
			j++;
			result.addMembers(monom);
		}
		return result;
	}

	public Polynomial mul(Polynomial P, Polynomial Q) {

		i = 0;
		result.cleanMembers();
		int nrP = P.nrOfmembers(), nrQ = Q.nrOfmembers();

		while (i < nrP) {
			double coefP = P.getPos(i).getCoef(), powP = P.getPos(i).getPower();
			i++;
			j = 0;
			while (j < nrQ) {
				double coefQ = Q.getPos(j).getCoef(), powQ = Q.getPos(j).getPower();
				monom = new Monomial(coefP * coefQ, powP + powQ);
				result.addMembers(monom);
				j++;
			}
		}
		return result;
	}

	public String div(Polynomial P, Polynomial Q) {

		Operations o = new Operations();
		i = 0;
		result.cleanMembers();

		try {
			while (P.getPos(0).getPower() >= Q.getPos(0).getPower()) {

				double coefQ = Q.getPos(0).getCoef(), powQ = Q.getPos(0).getPower();
				Monomial tmp = new Monomial(coefQ, powQ);
				Monomial tmp2 = P.getPos(i);
				double coefTmp = tmp2.div(tmp).getCoef(), powTmp = P.getPos(i).div(tmp).getPower();
				monom = new Monomial(coefTmp, powTmp);
				Polynomial aux = new Polynomial();
				aux.addMembers(monom);
				result.addMembers(monom);
				Polynomial aux2 = new Polynomial();
				aux2 = o.mul(Q, aux);
				P = o.sub(P, aux2);
				P.removePos(0);
			}
		} catch (Exception E) {
		}
		;
		if (P.isEmpty() == true)
			return result.toStringDouble();
		else
			return result.toStringDouble() + " R = " + P.toStringDouble();
	}

	public Polynomial deriv(Polynomial P) {

		int i = 0;
		result.cleanMembers();
		int nrP = P.nrOfmembers();

		while (i < nrP) {
			double coefP = P.getPos(i).getCoef(), powP = P.getPos(i).getPower();
			i++;
			if (powP != 0) {
				monom = new Monomial(coefP * powP, powP - 1);
				result.addMembers(monom);
			}
		}
		return result;
	}

	public Polynomial integ(Polynomial P) {

		i = 0;
		result.cleanMembers();
		int nrP = P.nrOfmembers();
		try {
			while (i < nrP) {
				double coefP = P.getPos(i).getCoef(), powP = P.getPos(i).getPower();
				i++;
				monom = new Monomial(coefP / (powP + 1), powP + 1);
				result.addMembers(monom);
			}
		} catch (Exception E) {
		}
		;
		return result;
	}

	public double findValue(Polynomial P, int valx) {
		double val = 0;
		int i = 0;

		while (i < P.nrOfmembers()) {
			val += Math.pow(valx, P.getPos(i).getPower()) * P.getPos(i).getCoef();
			i++;
		}
		return val;
	}

}
