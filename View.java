package poly_calc;
/*
 * Katona David - 30224
 */
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class View extends JFrame {

	private static final long serialVersionUID = 1L;
	String toShow = ""; // Result to show
	protected Polynomial polinomP = new Polynomial(), polinomQ = new Polynomial();
	final static boolean shouldWeightX = true;
	final static boolean RIGHT_TO_LEFT = false;
	boolean readP = true, readQ = true;

	Controller controller = new Controller(this);
	JButton add, sub, mul, div, deriv, integ, findValue;
	JButton onePoly, twoPoly, instr, clear;
	JTextField textBoxP, textBoxQ, textBoxRes;
	JLabel poliP, poliQ, result;

	GridBagConstraints c = new GridBagConstraints();

	public void addComponentsToPane(Container pane) {

		if (RIGHT_TO_LEFT) {
			pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		}
		pane.setLayout(new GridBagLayout()); // Layout form

		/*
		 * 2 Polynomials button and actions - If button is pressed, it toggles to 2
		 * polynomial mode - add, sub, div and mul are enabled; deriv, int and findValue
		 * are disabled
		 */
		twoPoly = new JButton("2 Polynomials");
		twoPoly.setVisible(false);
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		twoPoly.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(ActionEvent event) {
				// Enable and show buttons for 2 polynomials
				add.setEnabled(true);
				sub.setEnabled(true);
				mul.setEnabled(true);
				div.setEnabled(true);
				add.setVisible(true);
				sub.setVisible(true);
				mul.setVisible(true);
				div.setVisible(true);
				// Disable and hide obsolete buttons for 2 polynomials
				deriv.setEnabled(false);
				integ.setEnabled(false);
				findValue.setEnabled(false);
				deriv.setVisible(false);
				integ.setVisible(false);
				findValue.setVisible(false);
				// Enable polynomial Q
				polinomQ.cleanMembers();
				poliQ.setVisible(true);
				readQ = true;
				textBoxQ.setEnabled(true);
				textBoxQ.setText(null);
				// Toggle button visibility
				twoPoly.setVisible(false);
				onePoly.setVisible(true);
				textBoxRes.setText(null);
			}
		});
		twoPoly.setSelected(true);
		pane.add(twoPoly, c);

		/*
		 * 1 Polynomial button and actions If button is pressed, it toggles to 1
		 * polynomial mode add, sub, div and mul are disabled deriv, int and findValue
		 * are enabled
		 */
		onePoly = new JButton("1 Polynomial");
		onePoly.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent event) {
				// Disable and hide obsolete buttons for 1 polynomial
				add.setEnabled(false);
				sub.setEnabled(false);
				mul.setEnabled(false);
				div.setEnabled(false);
				add.setVisible(false);
				sub.setVisible(false);
				mul.setVisible(false);
				div.setVisible(false);
				// Enable and show buttons for 1 polynomial
				deriv.setEnabled(true);
				integ.setEnabled(true);
				findValue.setEnabled(true);
				deriv.setVisible(true);
				integ.setVisible(true);
				findValue.setVisible(true);
				// Disable polynomial Q
				textBoxQ.setEnabled(false);
				textBoxQ.setText(null);
				readQ = false;
				/// Toggle button visibility
				twoPoly.setVisible(true);
				onePoly.setVisible(false);
				textBoxRes.setText(null);
			}
		});
		onePoly.setSelected(false);
		pane.add(onePoly, c);

		// Instructions button
		instr = new JButton("Instructions");
		c.gridy = 0;
		c.gridx = 1;
		instr.addActionListener(controller);
		pane.add(instr, c);
		// Clear button
		clear = new JButton("C");
		clear.addActionListener(controller);
		clear.setFont(clear.getFont().deriveFont(12.0f));
		c.anchor = GridBagConstraints.EAST;
		c.weightx = 1;
		c.gridwidth = 4;
		c.gridy = 0;
		c.gridx = 2;
		pane.add(clear, c);
		// + button
		add = new JButton("+");
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 1;
		pane.add(add, c);
		add.addActionListener(controller);
		// - button
		sub = new JButton("-");
		c.gridx = 1;
		sub.addActionListener(controller);
		pane.add(sub, c);
		// * button
		mul = new JButton("*");
		c.gridx = 2;
		mul.addActionListener(controller);
		pane.add(mul, c);
		// / button
		div = new JButton("/");
		div.addActionListener(controller);
		c.gridx = 3;
		pane.add(div, c);
		// P' button
		deriv = new JButton("P'");
		c.gridy = 1;
		c.gridx = 0;
		deriv.addActionListener(controller);
		pane.add(deriv, c);
		// Integrate button - using unicode symbol
		integ = new JButton("\u0283 P dx");
		c.gridx = 1;
		integ.addActionListener(controller);
		pane.add(integ, c);
		// P(x) button
		findValue = new JButton("P(x)");
		findValue.addActionListener(controller);
		c.gridx = 2;
		pane.add(findValue, c);
		// P: label
		poliP = new JLabel("P: ");
		c.gridx = 0;
		c.gridy = 2;
		pane.add(poliP, c);
		// P text box
		textBoxP = new JTextField(15);
		c.gridx++;
		pane.add(textBoxP, c);
		// Q: label
		poliQ = new JLabel("Q: ");
		c.gridx = 0;
		c.gridy = 3;
		pane.add(poliQ, c);
		// Q text box
		textBoxQ = new JTextField(15);
		c.gridx++;
		pane.add(textBoxQ, c);
		// Result: label
		result = new JLabel("Result: ", JLabel.LEFT);
		c.gridx = 0;
		c.gridy = 4;
		pane.add(result, c);
		result.setFont(result.getFont().deriveFont(15.0f));
		// Result text box
		textBoxRes = new JTextField(15);
		c.gridx = 1;
		textBoxRes.setEditable(false); // only display
		textBoxRes.setBackground(new Color(180, 190, 210));
		pane.add(textBoxRes, c);
	}

	private void createGUI() {

		JFrame frame = new JFrame("Polynomial calculator");
		frame.getContentPane().setBackground(new Color(60, 160, 230));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addComponentsToPane(frame.getContentPane());
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) {

		View view = new View();
		view.createGUI();
	}
}
