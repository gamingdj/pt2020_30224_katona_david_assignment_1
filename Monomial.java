package poly_calc;
/*
 * Katona David - 30224
 */
public class Monomial {

	private double coef, power;

	public Monomial(double coef, double power) {
		super();
		this.coef = coef;
		this.power = power;
	}

	public double getCoef() {
		return coef;
	}

	public void setCoef(double coef) {
		this.coef = coef;
	}

	public double getPower() {
		return power;
	}

	public void setPower(double power) {
		this.power = power;
	}

	public Monomial div(Monomial aux) {
		return new Monomial(this.coef / aux.getCoef(), power - aux.getPower());
	}
}