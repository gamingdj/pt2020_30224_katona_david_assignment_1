package poly_calc;
/*
 * Katona David - 30224
 */
import static org.junit.Assert.*;
import org.junit.*;

public class JUnitTest {

	private static Monomial m;
	private static Polynomial p, q, res;
	private static Operations o = new Operations();
	private static int tested = 0, succeeded = 0;

	public JUnitTest() {

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("\nOut of " + tested + " tests, " + succeeded + " were successful");
	}

	@Before
	public void setUp() throws Exception {
		tested++;
		p = new Polynomial();
		p.cleanMembers();
		q = new Polynomial();
		q.cleanMembers();
		res = new Polynomial();
		res.cleanMembers();
	}

	@Test
	public void testAddition() {
		m = new Monomial(2, 3);
		p.addMembers(m);
		m = new Monomial(3, 2);
		p.addMembers(m);
		// p = 2x^3+3x^2
		m = new Monomial(4, 3);
		q.addMembers(m);
		// q = 4x^3
		res = o.add(p, q);
		// res = 6x^3+3x^2
		assertTrue(res.getPos(0).getCoef() == 6.0d && res.getPos(0).getPower() == 3.0d && res.getPos(1).getCoef() == 3.0d);
		System.out.println("Addition successful");
		succeeded++;
	}

	@Test
	public void testSubstraction() {
		m = new Monomial(5, 3);
		p.addMembers(m);
		m = new Monomial(2, 3);
		q.addMembers(m);
		m = new Monomial(3, 3);
		res = o.sub(p, q);
		assertTrue(res.getPos(0).getCoef() == m.getCoef() && res.getPos(0).getPower() == m.getPower());
		System.out.println("Substraction successful");
		succeeded++;
	}

	@Test
	public void testMultiplication() {
		m = new Monomial(5, 3);
		p.addMembers(m);
		m = new Monomial(2, 3);
		q.addMembers(m);
		m = new Monomial(10, 6);
		res = o.mul(p, q);
		assertTrue(res.getPos(0).getCoef() == m.getCoef() && res.getPos(0).getPower() == m.getPower());
		System.out.println("Multiplication successful");
		succeeded++;
	}

	@Test
	public void testDerivate() {
		m = new Monomial(6, 3);
		p.addMembers(m);
		m = new Monomial(18, 2);
		res = o.deriv(p);
		assertTrue(res.getPos(0).getCoef() == m.getCoef() && res.getPos(0).getPower() == m.getPower());
		System.out.println("Derivation successful");
		succeeded++;
	}

	@Test
	public void testIntegrate() {
		m = new Monomial(4, 3);
		p.addMembers(m);
		m = new Monomial(1, 4);
		res = o.integ(p);
		assertTrue(res.getPos(0).getCoef() == m.getCoef() && res.getPos(0).getPower() == m.getPower());
		System.out.println("Integration successful");
		succeeded++;
	}

	@Test
	public void testFindValue() {
		m = new Monomial(6, 3);
		p.addMembers(m);
		assertTrue(o.findValue(p, 3) == 162.0d);
		System.out.println("Finding value in x successful");
		succeeded++;
	}

}