package poly_calc;

/*
 * Katona David - 30224
 */
import java.util.ArrayList;

public class Polynomial {

	private ArrayList<Monomial> members = new ArrayList<Monomial>();
	private int nr = 0;

	public void addMembers(Monomial monom) {
		members.add(monom);
		nr++;
	}

	public Monomial getPos(int i) {
		return members.get(i);
	}

	public void removePos(int i) {
		members.remove(i);
	}

	public void cleanMembers() {
		members.clear();
		nr = 0;
	}

	public int nrOfmembers() {
		return nr;
	}

	public boolean isEmpty() {
		if (members.isEmpty())
			return true;
		return false;
	}

	@Override
	public String toString() {
		String polString = "", powstr = "", coefstr = "";
		boolean isNum = false;

		for (Monomial m : members) {
			int coef = (int) m.getCoef();
			int pow = (int) m.getPower();

			if (coef == 0)
				continue;
			if ((coef != 1) && (coef != -1))
				coefstr = String.valueOf(coef);
			else if (coef == 1) {
				coefstr = "";
				isNum = true;
			} else if (coef == -1) {
				coefstr = "-";
			}
			if ((pow != 0) && (pow != 1))
				powstr = "x^" + pow;
			else if (pow == 1)
				powstr = "x";

			if (coef == 1 && pow == 0)
				coefstr = "1";
			if (coef == -1 && pow == 0)
				coefstr = "-1";
			polString += coefstr + powstr + "+";
			polString = polString.replace("+-", "-");
		}
		if (polString.compareTo("") == 0) {
			if (isNum)
				polString = "1";
			else
				polString = "0";
		} else if (polString.length() > 0)
			polString = polString.substring(0, polString.length() - 1);
		polString = polString.replace("+-", "-");
		if (polString.compareTo("-") == 0)
			polString = "-1";
		if (members.isEmpty()) {
			polString = "0";
		}

		return polString;
	}

	// For representing floating point coefficient polynomials
	public String toStringDouble() {
		String polString = "", powstr = "", coefstr = "";

		for (Monomial m : members) {
			double coef = m.getCoef();
			double pow = m.getPower();
			
			if (coef == 0)
				continue;
			if (coef != 1 && coef != -1)
				coefstr = String.valueOf(String.format("%.3g ", coef));
			else if (coef == 1)
				coefstr = "";
			else if (coef == -1)
				coefstr = "-";
			if (pow != 0 && pow != 1)
				powstr = "x^" + (int) pow;
			else if (pow == 1)
				powstr = "x";
			else if (pow == 0)
				powstr = "";
			polString += coefstr + powstr + "+";
		}

		if (polString.compareTo("") == 0 || members.isEmpty() == true)
			polString = "0";
		else if (polString.length() > 0)
			polString = polString.substring(0, polString.length() - 1);
		polString = polString.replace("+-", "-");

		return polString;
	}
}