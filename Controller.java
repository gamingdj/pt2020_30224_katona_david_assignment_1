package poly_calc;

/*
 * Katona David - 30224
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

public class Controller implements ActionListener {

	private View view;
	Operations op = new Operations();
	private String instructions = "Use the top left button to toggle between the calculator's modes.\n"
			+ "In 2 Polynomials mode, enter P and Q polynomials, then select the operation.\n"
			+ "In 1 Polynomial mode, enter the P polynomial, then select the operation.\n"
			+ "Use the 'C' button to clear and reset the calculator.\n" + "\nFormat: ax^n+bx^n-1+...+yx^1+z\n"
			+ "Example: 2x^2, 5x^3+4x^2    (Don't leave space between the members)\n";
	private String invalidFormat = "Invalid format! Read instruction...";
	private String invalidDivGrade = "Grade of P has to be greater than or equal to Q!";
	private int x, coef = 0, pow = 0;
	private StringTokenizer xTok, pTok, mTok;
	private String stringP, stringQ;

	public Controller(View v) {
		this.view = v;
	}

	private void showMsg(String errorMsg) {
		JOptionPane.showMessageDialog(null, errorMsg, "Error", JOptionPane.ERROR_MESSAGE);
	}

	public void actionPerformed(ActionEvent event) {
		// Read P
		if (view.readP == true) {
			try {
				view.polinomP.cleanMembers();
				stringP = view.textBoxP.getText();
				if (stringP.startsWith("-")) {

					if (stringP.charAt(1) == 'x') {
						coef = -1;
					}

					else if (Character.isDigit(stringP.charAt(1))) {
						stringP = stringP.substring(1);
						StringTokenizer first = new StringTokenizer(stringP, "x^");
						String theCoef = first.nextToken();
						coef = -Integer.parseInt(theCoef);
						stringP = stringP.substring(theCoef.length());
					} else
						showMsg(invalidFormat);
				}

				else if (stringP.startsWith("x")) {
					coef = 1;
				}

				else if (Character.isDigit(stringP.charAt(0))) {
					StringTokenizer first = new StringTokenizer(stringP, "x^");
					String theCoef = first.nextToken();
					coef = Integer.parseInt(theCoef);
					stringP = stringP.substring(theCoef.length());
				} else
					showMsg(invalidFormat);

				// Separating the String
				xTok = new StringTokenizer(stringP, "x^");
				while (xTok.hasMoreElements()) {
					String tmp = xTok.nextToken();
					pTok = new StringTokenizer(tmp, "+");
					mTok = new StringTokenizer(tmp, "-");
					if (tmp.indexOf("-") == -1) // +
					{
						String thePow = pTok.nextToken();
						pow = Integer.parseInt(thePow);
						Monomial m = new Monomial(coef, pow);
						view.polinomP.addMembers(m);
						if (pTok.hasMoreElements()) {
							String theCoef = pTok.nextToken();
							coef = Integer.parseInt(theCoef);
						}

						else
							coef = 1;
					}

					else {
						String thePow = mTok.nextToken();
						pow = Integer.parseInt(thePow);
						Monomial m = new Monomial(coef, pow);
						view.polinomP.addMembers(m);

						if (mTok.hasMoreElements()) {
							String theCoef = mTok.nextToken();
							coef = -Integer.parseInt(theCoef);
						}

						else
							coef = -1;
					}
				}
				view.readP = false;
			} catch (Exception E) {
				view.polinomP.cleanMembers();
				stringP = "";
				showMsg(invalidFormat);
			}
			;
		}
		// Read Q
		if (view.twoPoly.isSelected() && view.readQ) {
			try {
				view.polinomQ.cleanMembers();
				stringQ = view.textBoxQ.getText();
				pow = 0;
				coef = 0;
				if (stringQ.startsWith("-")) {
					if (stringQ.charAt(1) == 'x') {
						coef = -1;
					}

					else if (Character.isDigit(stringQ.charAt(1))) {
						stringQ = stringQ.substring(1);
						StringTokenizer first = new StringTokenizer(stringQ, "x^");
						String theCoef = first.nextToken();
						coef = -Integer.parseInt(theCoef);
						stringQ = stringQ.substring(theCoef.length());
					}

					else
						showMsg(invalidFormat);
				}

				else if (stringQ.startsWith("x"))
					coef = 1;
				else if (Character.isDigit(stringQ.charAt(0))) {
					StringTokenizer first = new StringTokenizer(stringQ, "x^");
					String theCoef = first.nextToken();
					coef = Integer.parseInt(theCoef);
					stringQ = stringQ.substring(theCoef.length());
				} else
					showMsg(invalidFormat);
				// Separating the String
				xTok = new StringTokenizer(stringQ, "x^");
				while (xTok.hasMoreElements()) {
					String tmp = xTok.nextToken();
					pTok = new StringTokenizer(tmp, "+");
					mTok = new StringTokenizer(tmp, "-");
					if (tmp.indexOf("-") == -1) // +
					{
						String thePow = pTok.nextToken();
						pow = Integer.parseInt(thePow);
						Monomial m = new Monomial(coef, pow);
						view.polinomQ.addMembers(m);
						if (pTok.hasMoreElements()) {
							String theCoef = pTok.nextToken();
							coef = Integer.parseInt(theCoef);
						} else
							coef = 1;
					} else {
						String thePow = mTok.nextToken();
						pow = Integer.parseInt(thePow);
						Monomial m = new Monomial(coef, pow);
						view.polinomQ.addMembers(m);

						if (mTok.hasMoreElements()) {
							String theCoef = mTok.nextToken();
							coef = -Integer.parseInt(theCoef);
						} else
							coef = -1;
					}
				}
				view.readQ = false;
			} catch (Exception E) {
				view.polinomQ.cleanMembers();
				stringQ = "";
				showMsg(invalidFormat);
			}
			;
		}
		// Button actions
		// Instructions button
		if (event.getSource() == view.instr)
			JOptionPane.showMessageDialog(view.instr, instructions, "Instructions", JOptionPane.PLAIN_MESSAGE);
		// Clear button
		if (event.getSource() == view.clear) {
			view.readP = true;
			view.readQ = true;
			view.textBoxQ.setText("");
			view.textBoxP.setText("");
			view.toShow = "";
			view.polinomP.cleanMembers();
			view.polinomQ.cleanMembers();
		}
		// + button
		if (event.getSource() == view.add)
			view.toShow = op.add(view.polinomP, view.polinomQ).toString();
		// - button
		if (event.getSource() == view.sub)
			view.toShow = op.sub(view.polinomP, view.polinomQ).toString();
		// * button
		if (event.getSource() == view.mul)
			view.toShow = op.mul(view.polinomP, view.polinomQ).toString();
		// / button
		if (event.getSource() == view.div)
			if (view.polinomP.getPos(0).getPower() < view.polinomQ.getPos(0).getPower()) {
				showMsg(invalidDivGrade);
				view.toShow = "0, R = " + view.polinomP.toString();
			} else
				view.toShow = op.div(view.polinomP, view.polinomQ);
		// P' button
		if (event.getSource() == view.deriv)
			view.toShow = op.deriv(view.polinomP).toString();
		// Integrate button
		if (event.getSource() == view.integ)
			view.toShow = op.integ(view.polinomP).toStringDouble();
		// P(x) button
		if (event.getSource() == view.findValue) {
			x = Integer.parseInt(JOptionPane.showInputDialog(view.findValue, "Enter x: ", 0));
			view.toShow = String.valueOf(op.findValue(view.polinomP, x));
		}

		view.textBoxRes.setText(view.toShow);
	}
}
